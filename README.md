AWS deployment
====================================

AWS Cassandra deployment.

## Ansible Playbook

### Описание

Ansible Playbook для быстрого развёртывания приложений.

### Использование

#### Установка

Инструкция:
* [MacOS]
* [Debian]
* [Ubuntu]
* [Прочие]

> Рекомендуется использовать Ansible только на Unix-подобных системах (MacOS, Linux).
>
> Если же у вас Windows - используйте виртуальную машину (это проще и быстрее, чем установить Ansible на Windows).

#### Подготовка

> Для удобства укажите в файле `/etc/hosts` адреса ваших серверов и их короткие названия, например:
>
> ```
> 1.1.1.1   node_1
> 2.2.2.2   node_2
> 3.3.3.3   node_3
> ```
>
> Тогда вместо ip адресов в файле `hosts` и, соответственно, в названиях файлов в `host_vars` (см. пункты далее)
> вы сможете использовать только названия серверов (в данном примере: `node_1` вместо `1.1.1.1`).
>
> Попробуйте соединиться с серверами по ssh, используя их названия вместо полных адресов.

Для запуска playbook'а:

1. Определите тип дистрибутива Linux вашего сервера ([полный список]):
    1. Debian-подобные - в дальнейшем `debian`:
        1. Debian
        2. Ubuntu
    2. RedHat-подобные - в дальнейшем `redhat`:
        1. RedHat
        2. CentOS

2. Отредактируйте содержимое файла `hosts` в соответствии с комментариями внутри

3. Откройте каталог `host_vars`:
    1. Переименуйте файлы в `<ip_адрес_сервера_N>.yaml` (или `node_N.yaml`, см. примечание выше)
    2. Отредактируйте содержимое файлов в соответствии с комментариями внутри

4. Отредактируйте содержимое файла `playbook.yaml` в соответствии с комментариями внутри

#### Запуск

Запустите playbook с помощью команды `ansible-playbook -i hosts --key-file <путь_к_вашему_ssh_ключу> playbook.yaml`.

[MacOS]:    https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#latest-releases-on-macos
[Debian]:   https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#latest-releases-via-apt-debian
[Ubuntu]:   https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#latest-releases-via-apt-ubuntu
[Прочие]:   https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html

[полный список]: https://ru.wikipedia.org/wiki/%D0%A1%D0%BF%D0%B8%D1%81%D0%BE%D0%BA_%D0%B4%D0%B8%D1%81%D1%82%D1%80%D0%B8%D0%B1%D1%83%D1%82%D0%B8%D0%B2%D0%BE%D0%B2_Linux